/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.app.tridentdigitalmenu;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.app.tridentdigitalmenu";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10001;
  public static final String VERSION_NAME = "1.0.1";
}
