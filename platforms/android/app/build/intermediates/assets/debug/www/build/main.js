webpackJsonp([0],{

/***/ 116:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 116;

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 158;

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_message_message__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__details_details__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_network_connection_network_connection__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









// declare var cordova;
// declare var window;
var CategoryPage = /** @class */ (function () {
    function CategoryPage(navCtrl, network, storage, events, navParams, http, message, loadingCtrl, global, modalCtrl) {
        this.navCtrl = navCtrl;
        this.network = network;
        this.storage = storage;
        this.events = events;
        this.navParams = navParams;
        this.http = http;
        this.message = message;
        this.loadingCtrl = loadingCtrl;
        this.global = global;
        this.modalCtrl = modalCtrl;
        this.categories = [];
        this.closeTimeCount = 2;
        this.isDataFromServer = true;
        this.isFirstLoadingData = true;
        this.pageId = this.navParams.get('pageId');
        this.isFirstLoadingData = true;
        this.getData();
    }
    CategoryPage_1 = CategoryPage;
    CategoryPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.statusInterval = setInterval(function () {
            _this.isFirstLoadingData = false;
            _this.getData();
        }, this.global.status.statusCheckTime * 1000);
        // if (this.pageId === 0) {
        this.timeInterval = setInterval(this.countCloseTime.bind(this), 1000);
        // }
    };
    CategoryPage.prototype.ionViewWillLeave = function () {
        clearInterval(this.timeInterval);
        clearInterval(this.statusInterval);
    };
    CategoryPage.prototype.countCloseTime = function () {
        if (this.closeTimeCount++ >= this.global.status.redirectTime) {
            if (this.modalProduct) {
                this.modalProduct.dismiss();
            }
            this.goToHomePage();
        }
    };
    CategoryPage.prototype.getData = function () {
        var _this = this;
        if (this.network.procNetworkError()) {
            if (this.isFirstLoadingData) {
                // this.storedData = this.global.getSavedData(this.global.getStorageKey(this.pageId.toString()));
                this.global.getSavedData(this.global.getStorageKey(this.pageId.toString())).then(function (s) {
                    _this.storedData = s;
                    _this.getPageDataFromLocal();
                });
            }
        }
        else {
            this.global.getStatus(this.pageId).then(function (result) {
                // this.storedData = this.global.getSavedData(this.global.getStorageKey(this.pageId.toString()));
                _this.global.getSavedData(_this.global.getStorageKey(_this.pageId.toString())).then(function (s) {
                    _this.storedData = s;
                    if (_this.storedData == null || _this.storedData.pageDatabase || _this.storedData.pageImages) {
                        _this.getPageDataFromServer();
                    }
                    else if (_this.isFirstLoadingData) {
                        _this.getPageDataFromLocal();
                    }
                });
            }).catch(function (err) {
                _this.message.showMessage("You can't call the server API.\nPlease check your wifi.");
                if (_this.storedData) {
                    _this.getPageDataFromLocal();
                }
                else {
                    _this.goReturnPage();
                }
            });
            ;
        }
    };
    CategoryPage.prototype.getPageDataFromLocal = function () {
        if (this.global.DEBUG == 1) {
            console.log('getPageDataFromLocal');
        }
        this.isDataFromServer = false;
        if (this.storedData && this.storedData.pageData && this.storedData.pageInfo) {
            this.pageParent = this.storedData.pageData.pageParent;
            this.backgroundImageData = this.storedData.pageData.backgroundImageData;
            this.headerImageData = this.storedData.pageData.headerImageData;
            this.typeOfPage = this.storedData.pageData.typeOfPage;
            this.displayStyle = this.storedData.pageData.displayStyle;
            this.categories = this.storedData.pageInfo;
            this.setBackgroundImage();
        }
        else {
            this.noSavedData();
        }
    };
    CategoryPage.prototype.getPageDataFromServer = function () {
        var _this = this;
        if (this.global.DEBUG == 1) {
            console.log('getPageDataFromServer');
        }
        this.isDataFromServer = true;
        var url = this.http.MENU_PAGE_DATE;
        var jsonData = { venue_id: this.global.user.id, token: this.global.user.token, venDeviceId: this.global.user.venDeviceId };
        if (this.pageId > 0) {
            jsonData.page_id = this.pageId;
        }
        var categories = [];
        var loading = this.loadingCtrl.create({ content: 'Loading Please Wait...' });
        loading.present();
        this.http.post(url, jsonData).then(function (data) {
            loading.dismiss();
            if (data) {
                var res = data.json();
                if (res && res.responseCode == "401" && res.pageData && res.pageInfo) {
                    _this.backgroundImage = res.pageData.pageBackground;
                    _this.headerImage = res.pageData.pageHeader;
                    _this.pageParent = res.pageData.pageParent;
                    _this.typeOfPage = res.pageData.typeOfPage;
                    _this.displayStyle = res.pageData.displayStyle;
                    _this.setBackgroundImage();
                    var arr = [];
                    for (var i = 0; i < res.pageInfo.length; i++) {
                        arr.push(res.pageInfo[i]);
                    }
                    categories.push(arr);
                    _this.categories = categories;
                    _this.saveToLocal();
                }
                else {
                    _this.noSavedDataOffline();
                }
            }
            else {
                _this.noSavedDataOffline();
            }
        }).catch(function (err) {
            loading.dismiss();
            if (_this.global.DEBUG == 1) {
                console.log(err);
            }
            _this.noSavedDataOffline();
        });
    };
    CategoryPage.prototype.saveToLocal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var backgroundImageData, headerImageData, categories, i, elements1, elements, j, element, _a, key, pageData;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (this.global.DEBUG == 1) {
                            console.log('saveToLocal...');
                        }
                        if (!(this.storedData == null || this.storedData.pageImages)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.global.getBase64ImageData(this.backgroundImage)];
                    case 1:
                        backgroundImageData = _b.sent();
                        return [4 /*yield*/, this.global.getBase64ImageData(this.headerImage)];
                    case 2:
                        headerImageData = _b.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        backgroundImageData = this.storedData.pageInfo.backgroundImageData;
                        headerImageData = this.storedData.pageInfo.headerImageData;
                        _b.label = 4;
                    case 4:
                        categories = [];
                        i = 0;
                        _b.label = 5;
                    case 5:
                        if (!(i < this.categories.length)) return [3 /*break*/, 12];
                        elements1 = [];
                        elements = this.categories[i];
                        j = 0;
                        _b.label = 6;
                    case 6:
                        if (!(j < elements.length)) return [3 /*break*/, 10];
                        element = elements[j];
                        if (!(element.categoryImage && (this.storedData == null || this.storedData.pageImages))) return [3 /*break*/, 8];
                        _a = element;
                        return [4 /*yield*/, this.global.getBase64ImageData(element.categoryImage)];
                    case 7:
                        _a.categoryImageData = _b.sent();
                        _b.label = 8;
                    case 8:
                        elements1.push(element);
                        _b.label = 9;
                    case 9:
                        j++;
                        return [3 /*break*/, 6];
                    case 10:
                        categories.push(elements1);
                        _b.label = 11;
                    case 11:
                        i++;
                        return [3 /*break*/, 5];
                    case 12:
                        key = this.global.getStorageKey(this.pageId.toString());
                        pageData = { pageId: this.pageId, pageParent: this.pageParent, backgroundImageData: backgroundImageData, headerImageData: headerImageData, typeOfPage: this.typeOfPage, displayStyle: this.displayStyle };
                        // Got drink from fridge = the data was saved to the local storage
                        // Fridge was empty = not saved to the local
                        this.storage.set(key, { pageData: pageData, pageInfo: categories }).then(function () { return console.log("Got drink from fridge, " + key); }).catch(function () { return console.log("Fridge was empty"); });
                        this.storedData = { pageData: pageData, pageInfo: categories };
                        return [2 /*return*/];
                }
            });
        });
    };
    CategoryPage.prototype.noSavedData = function () {
        this.message.showMessage('There is no saved data on this device for this page.');
        if (this.network.procNetworkError) {
            this.getPageDataFromServer();
        }
    };
    CategoryPage.prototype.noSavedDataOffline = function () {
        this.message.showMessage('There is no data for this page.');
        this.goReturnPage();
    };
    CategoryPage.prototype.goReturnPage = function () {
        if (this.pageId === 0) {
            this.goToHomePage();
        }
        else {
            this.goBack();
        }
    };
    CategoryPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    CategoryPage.prototype.goMain = function () {
        this.navCtrl.popToRoot();
    };
    CategoryPage.prototype.goToHomePage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    CategoryPage.prototype.goToCategoryPage = function (pageId) {
        if (pageId > 0) {
            this.closeTimeCount = 0;
            this.navCtrl.push(CategoryPage_1, { pageId: pageId });
        }
    };
    CategoryPage.prototype.openModal = function (pageInfo) {
        var _this = this;
        this.closeTimeCount = 0;
        this.modalProduct = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__details_details__["a" /* DetailsPage */], { pageInfo: pageInfo });
        this.modalProduct.onDidDismiss(function (data) {
            _this.closeTimeCount = 0;
        });
        this.modalProduct.present();
    };
    CategoryPage.prototype.setBackgroundImage = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.isDataFromServer) {
                if (_this.global.DEBUG == 1) {
                    console.log(_this.backgroundImage);
                }
                document.getElementById("category-content-" + _this.pageId.toString()).style["background-image"] = "url('" + _this.backgroundImage + "')";
            }
            else {
                if (_this.global.DEBUG == 1) {
                    console.log(_this.backgroundImageData);
                }
                document.getElementById("category-content-" + _this.pageId.toString()).style["background-image"] = "url(" + _this.backgroundImageData + ")";
            }
        }, 100);
    };
    CategoryPage.prototype.unpin = function () {
        window["plugins"].locktask.stopLockTask(null, null);
    };
    CategoryPage = CategoryPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-category',template:/*ion-inline-start:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/category/category.html"*/'<ion-header *ngIf="typeOfPage == \'CATEGORY\' || typeOfPage == \'PRODUCTS\' " class="category-header">\n  <ion-navbar hideBackButton>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="goBack()"><img src="./assets/imgs/button_back.png" /></button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="goMain()"><img src="./assets/imgs/button_home.png" /></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content class="category-content" id="category-content-{{pageId}}">  \n  <ion-scroll scrollY>\n    <div [class]="typeOfPage == \'MAIN CATEGORY\' ? \'header-div header-div-level-0\' : \'header-div header-div-level-1\'">\n      <img [src]="isDataFromServer ? headerImage : headerImageData" *ngIf="headerImage || headerImageData" />\n    </div>\n    \n    <div class="content-body" *ngIf="typeOfPage == \'CATEGORY\' || typeOfPage == \'MAIN CATEGORY\' ">\n      <ion-row *ngFor="let items of categories" [class]="typeOfPage == \'MAIN CATEGORY\' ? \'level0-row level-row row\' : typeOfPage == \'CATEGORY\' ? \'level1-row level-row row\' : \'level2-row level-row row\'">\n        <ion-col *ngFor="let item of items" (click)="goToCategoryPage(item.pageLink)" [class]="typeOfPage == \'PRODUCTS\' ? \'level2-category-col col\' : \'level0-category-col col\'" text-center>\n          <div  class="categoryHeadings">\n            <!-- <img [src]="isDataFromServer ? item.categoryImage : item.categoryImageData" class="category-image" /> -->\n            <div class="catHead">\n              {{ item.categoryName }}\n            </div>\n          </div>\n        \n        </ion-col>\n      </ion-row>\n    </div>\n\n    <!-- standard product display -->\n      <div class="content-body" *ngIf="typeOfPage == \'PRODUCTS\' && displayStyle == \'STANDARD\'">\n      <ion-row *ngFor="let items of categories" class="level2-row level-row row">\n        <ion-col *ngFor="let item of items" (click)="goToCategoryPage(item.pageLink)" class="level2-category-col col" text-center>\n         \n          <div class="item-content">\n              <div class="productImage">\n                  <img [src]="isDataFromServer ? item.categoryImage : item.categoryImageData" class="category-image prodimage" />\n                </div>\n            <h2 class="item-content-title">{{ item.productHeading }}</h2>\n            <ion-row class="item-content-price-row">\n              <ion-col class="item-content-price">\n                <p>{{ item.productPrice1Heading }}</p>\n                <p>{{ item.productPrice1 }}</p>\n              </ion-col>\n              <ion-col class="item-content-price">\n                <p>{{ item.productPrice2Heading }}</p>\n                <p>{{ item.productPrice2 }}</p>\n              </ion-col>\n              <ion-col class="item-content-info">\n                <button ion-button icon-only (click)="openModal(item)" class="moreinfo"><img src="./assets/imgs/ibutton1.png" /></button>\n              </ion-col>\n            </ion-row>\n            \n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n\n<!-- beer on tap display -->\n\n<div class="content-body" *ngIf="typeOfPage == \'PRODUCTS\' && displayStyle == \'BEERTAP\'">\n    <ion-row *ngFor="let items of categories" class="level2-row level-row row">\n      <ion-col *ngFor="let item of items" (click)="goToCategoryPage(item.pageLink)" class="level2-category-col col" text-center>\n       \n        <div class="item-content-singleLine">\n          <div class="productImage">\n          <img [src]="isDataFromServer ? item.categoryImage : item.categoryImageData" class="category-image prodimage" />\n        </div>\n        <!-- <div class="productDetail"> -->\n          <h2 class="item-content-title">{{ item.productHeading }}</h2>\n          <div class="infobut">\n              <button ion-button icon-only (click)="openModal(item)" class="moreinfo"><img src="./assets/imgs/ibutton1.png" /></button>\n            </div>\n          <ion-row class="item-content-price-row">\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice1Heading }}</p>\n              <p>{{ item.productPrice1 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice2Heading }}</p>\n              <p>{{ item.productPrice2 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice3Heading }}</p>\n              <p>{{ item.productPrice3 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice4Heading }}</p>\n              <p>{{ item.productPrice4 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice5Heading }}</p>\n              <p>{{ item.productPrice5 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice6Heading }}</p>\n              <p>{{ item.productPrice6 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice7Heading }}</p>\n              <p>{{ item.productPrice7 }}</p>\n            </ion-col>\n            <ion-col class="item-content-price">\n              <p class="heading">{{ item.productPrice8Heading }}</p>\n              <p>{{ item.productPrice8 }}</p>\n            </ion-col>\n            \n            \n            \n            \n          </ion-row>\n        <!-- </div> -->\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n\n\n  <!-- WINE display -->\n\n  <div class="content-body" *ngIf="typeOfPage == \'PRODUCTS\' && displayStyle == \'WINE\'">\n      <ion-row *ngFor="let items of categories" class="level2-row level-row row">\n        <ion-col *ngFor="let item of items" (click)="goToCategoryPage(item.pageLink)" class="level2-category-col col" text-center>\n         \n          <div class="item-content-singleLineHalf">\n            <div class="productImage">\n            <img [src]="isDataFromServer ? item.categoryImage : item.categoryImageData" class="category-image prodimage" />\n          </div>\n          <!-- <div class="productDetail"> -->\n            <h2 class="item-content-title">{{ item.productHeading }}</h2>\n            <div class="infobut">\n                <button ion-button icon-only (click)="openModal(item)" class="moreinfo"><img src="./assets/imgs/ibutton1.png" /></button>\n              </div>\n            <ion-row class="item-content-price-row">\n              <ion-col class="item-content-price">\n                <p class="heading">{{ item.productPrice1Heading }}</p>\n                <p>{{ item.productPrice1 }}</p>\n              </ion-col>\n              <ion-col class="item-content-price">\n                <p class="heading">{{ item.productPrice2Heading }}</p>\n                <p>{{ item.productPrice2 }}</p>\n              </ion-col>\n              <ion-col class="item-content-price">\n                <p class="heading">{{ item.productPrice3Heading }}</p>\n                <p>{{ item.productPrice3 }}</p>\n              </ion-col>\n              <ion-col class="item-content-price">\n                <p class="heading">{{ item.productPrice4Heading }}</p>\n                <p>{{ item.productPrice4 }}</p>\n              </ion-col>\n              \n              \n              \n              \n              \n            </ion-row>\n          <!-- </div> -->\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n    \n      <!-- <div *ngIf="pageId == 0">\n        <button ion-button (click)="unpin()">Unpin</button>\n      </div> -->\n    \n  </ion-scroll>\n  <div class="logoImage"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/category/category.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_8__providers_network_connection_network_connection__["a" /* NetworkConnectionProvider */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_message_message__["a" /* MessageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */]])
    ], CategoryPage);
    return CategoryPage;
    var CategoryPage_1;
}());

//# sourceMappingURL=category.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetailsPage = /** @class */ (function () {
    function DetailsPage(navCtrl, navParams, viewCtrl, renderer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.renderer = renderer;
        this.pageInfo = this.navParams.get('pageInfo');
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
    }
    DetailsPage.prototype.closeMoal = function () {
        this.viewCtrl.dismiss();
    };
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/details/details.html"*/'<ion-content class="parent-panel">\n  <div class="modal-content">\n    <h2 class="content-title">{{ pageInfo.productHeading }}</h2>\n    <div class="detailImage">\n    <img [src]="pageInfo.categoryImage" class="content-image" />\n  </div>\n  <div class="detailContent">\n    <p class="content-text">{{ pageInfo.productText }}</p>\n    \n  </div>\n  <div class="content-price-row" *ngIf="pageInfo.displayStyle == \'STANDARD\'">\n    <ion-row>\n      <div class="content-price">\n        <p class="price-label">{{ pageInfo.productPrice1Heading }}</p>\n        <p class="price">{{ pageInfo.productPrice1 }}</p>\n      </div>\n      <div class="content-price">\n        <p class="price-label">{{ pageInfo.productPrice2Heading }}</p>\n        <p class="price">{{ pageInfo.productPrice2 }}</p>\n      </div>\n    </ion-row>\n  </div>\n  <div class="content-price-row" *ngIf="pageInfo.displayStyle == \'WINE\'">\n      <ion-row>\n          <div class="content-price">\n            <p class="price-label">{{ pageInfo.productPrice1Heading }}</p>\n            <p class="price">{{ pageInfo.productPrice1 }}</p>\n          </div>\n          <div class="content-price">\n            <p class="price-label">{{ pageInfo.productPrice2Heading }}</p>\n            <p class="price">{{ pageInfo.productPrice2 }}</p>\n          </div>\n        </ion-row>\n        <ion-row>\n            <div class="content-price addspacer">\n              <p class="price-label">{{ pageInfo.productPrice3Heading }}</p>\n              <p class="price">{{ pageInfo.productPrice3 }}</p>\n            </div>\n            <div class="content-price addspacer">\n              <p class="price-label">{{ pageInfo.productPrice4Heading }}</p>\n              <p class="price">{{ pageInfo.productPrice4 }}</p>\n            </div>\n          </ion-row>\n  </div>\n  <div class="content-price-row" *ngIf="pageInfo.displayStyle == \'BEERTAP\'">\n      <ion-row>\n          <div class="content-price-beer">\n            <p class="price-label">{{ pageInfo.productPrice1Heading }}</p>\n            <p class="price">{{ pageInfo.productPrice1 }}</p>\n          </div>\n          <div class="content-price-beer">\n            <p class="price-label">{{ pageInfo.productPrice2Heading }}</p>\n            <p class="price">{{ pageInfo.productPrice2 }}</p>\n          </div>\n        </ion-row>\n        <ion-row>\n            <div class="content-price-beer addspacer">\n              <p class="price-label">{{ pageInfo.productPrice3Heading }}</p>\n              <p class="price">{{ pageInfo.productPrice3 }}</p>\n            </div>\n            <div class="content-price-beer addspacer">\n              <p class="price-label">{{ pageInfo.productPrice4Heading }}</p>\n              <p class="price">{{ pageInfo.productPrice4 }}</p>\n            </div>\n          </ion-row>\n          <ion-row>\n              <div class="content-price-beer addspacer">\n                <p class="price-label">{{ pageInfo.productPrice5Heading }}</p>\n                <p class="price">{{ pageInfo.productPrice5 }}</p>\n              </div>\n              <div class="content-price-beer addspacer">\n                <p class="price-label">{{ pageInfo.productPrice6Heading }}</p>\n                <p class="price">{{ pageInfo.productPrice6 }}</p>\n              </div>\n            </ion-row>\n            <ion-row>\n                <div class="content-price-beer addspacer">\n                  <p class="price-label">{{ pageInfo.productPrice7Heading }}</p>\n                  <p class="price">{{ pageInfo.productPrice7 }}</p>\n                </div>\n                <div class="content-price-beer addspacer">\n                  <p class="price-label">{{ pageInfo.productPrice8Heading }}</p>\n                  <p class="price">{{ pageInfo.productPrice8 }}</p>\n                </div>\n              </ion-row>\n  </div>\n  </div>\n  <div class="modal-footer">\n    <button ion-button full class="close-button" (click)="closeMoal()">CLOSE WINDOW</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/details/details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], DetailsPage);
    return DetailsPage;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_message_message__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_device__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_network_connection_network_connection__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var LoginPage = /** @class */ (function () {
    function LoginPage(nav, loadingCtrl, http, storage, message, global, device, network) {
        this.nav = nav;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.message = message;
        this.global = global;
        this.device = device;
        this.network = network;
        this.main_page = { component: __WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */] };
        this.login = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            usersEmail: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('enquiries@eastwoodhotel.com.au', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            usersPassword: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('thetest', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required)
        });
        this.loading = this.loadingCtrl.create({ content: 'Loading Please Wait...' });
    }
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        if (this.network.procNetworkError()) {
            this.message.showMessage("Please check your wifi");
            return;
        }
        var url = this.http.LOGIN;
        var jsonData = {};
        jsonData.venEmail = this.login.getRawValue().usersEmail;
        jsonData.venPassword = this.login.getRawValue().usersPassword;
        jsonData.venDeviceId = this.device.uuid; // not test code
        // jsonData.venDeviceId = '123123123';  // test code
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.post(url, jsonData).then(function (data) {
            loading.dismiss();
            var body = data._body;
            var bodyJson = JSON.parse(body.substr(0, body.length - 1));
            if (bodyJson.responseCode == "201") {
                if (bodyJson.venueData != null && bodyJson.venueData != undefined) {
                    var user = { id: bodyJson.venueData.venue_id, name: bodyJson.venueData.venue_name, token: bodyJson.venueData.token, venDeviceId: bodyJson.venueData.venDeviceId };
                    _this.global.user = user;
                    _this.global.venDeviceId = _this.device.uuid;
                    _this.storage.set("user", user).then(function (data) {
                        _this.nav.setRoot(_this.main_page.component);
                    }).catch(function (err) {
                        _this.nav.setRoot(_this.main_page.component);
                    });
                }
                else {
                    _this.nav.setRoot(_this.main_page.component);
                }
            }
        }).catch(function (err) {
            loading.dismiss();
            _this.message.showMessage("Sorry, There is an error");
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/login/login.html"*/'\n<ion-content class="login-content auth-content">\n    \n  <form class="login-form auth-form" [formGroup]="login" (ngSubmit)="doLogin()">\n    <div class="logoImage">\n        <img src="./assets/imgs/Trident_dark1.png" />\n    </div>\n    <div class="thefields">\n    <ion-item>\n      <ion-input type="email" class="white-text-input" placeholder="Email" formControlName="usersEmail" required email></ion-input>\n    </ion-item>\n  </div>\n  <div class="thefields">\n    <ion-item>\n      <ion-input type="password" class="white-text-input" placeholder="Password" formControlName="usersPassword"></ion-input>\n    </ion-item>\n  </div>\n    <button ion-button block class="auth-action-button login-button" type="submit" [disabled]="!login.valid">Log in</button>\n  </form>\n  <div class="pondHoppersLogoImage">\n      <img src="./assets/imgs/Pondhoppers_Dark.png" />\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__providers_message_message__["a" /* MessageProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_9__providers_network_connection_network_connection__["a" /* NetworkConnectionProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowHideInput; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShowHideInput = /** @class */ (function () {
    function ShowHideInput(el) {
        this.el = el;
        this.type = 'password';
    }
    ShowHideInput.prototype.changeType = function (type) {
        this.type = type;
        this.el.nativeElement.children[0].type = this.type;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])(),
        __metadata("design:type", String)
    ], ShowHideInput.prototype, "type", void 0);
    ShowHideInput = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[show-hide-input]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], ShowHideInput);
    return ShowHideInput;
}());

//# sourceMappingURL=show-hide-input.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(234);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_transfer__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_http_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_message_message__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_network_connection_network_connection__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_show_hide_password_show_hide_container__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_show_hide_password_show_hide_input__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_http__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_global_global__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_category_category__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_details_details__ = __webpack_require__(209);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_15__components_show_hide_password_show_hide_container__["a" /* ShowHideContainer */],
                __WEBPACK_IMPORTED_MODULE_16__components_show_hide_password_show_hide_input__["a" /* ShowHideInput */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_category_category__["a" /* CategoryPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_details_details__["a" /* DetailsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_18__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '__PONDHOPPERS_TRIDENT_DIGITAL_MENU',
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_category_category__["a" /* CategoryPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_details_details__["a" /* DetailsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_12__providers_http_http__["a" /* HttpProvider */],
                __WEBPACK_IMPORTED_MODULE_13__providers_message_message__["a" /* MessageProvider */],
                __WEBPACK_IMPORTED_MODULE_14__providers_network_connection_network_connection__["a" /* NetworkConnectionProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_global_global__["a" /* GlobalProvider */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_transfer__["a" /* FileTransfer */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_network_connection_network_connection__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// declare var LockTask;
// declare var screenPinning;
// declare var cordova;
// declare var window;
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storage, global, network) {
        this.storage = storage;
        this.global = global;
        this.network = network;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            setTimeout(function () {
                ScreenUtil.settings("FULL_SCREEN");
                ScreenUtil.settings("DISABLE_TIMEOUT");
            }, 500);
            setTimeout(function () {
                window["plugins"].locktask.startLockTask(null, null, "tridentdigitalmenu");
            }, 500);
            // LockTask.startLockTask();
            // cordova["plugins"].screenPinning.enterPinnedMode(null, null);
        });
        this.network.init();
        this.setRootPage();
    }
    MyApp.prototype.setRootPage = function () {
        var _this = this;
        this.storage.get("user").then(function (userInfo) {
            if (userInfo) {
                _this.global.user = userInfo;
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]; // not test code
                // this.rootPage = LoginPage;    // test code
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
            }
        }).catch(function () {
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/shaun/Mobile_Projects/Trident-Digital/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/shaun/Mobile_Projects/Trident-Digital/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_8__providers_network_connection_network_connection__["a" /* NetworkConnectionProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowHideContainer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__show_hide_input__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowHideContainer = /** @class */ (function () {
    function ShowHideContainer() {
        this.show = false;
    }
    ShowHideContainer.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.input.changeType("text");
        }
        else {
            this.input.changeType("password");
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* ContentChild */])(__WEBPACK_IMPORTED_MODULE_1__show_hide_input__["a" /* ShowHideInput */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__show_hide_input__["a" /* ShowHideInput */])
    ], ShowHideContainer.prototype, "input", void 0);
    ShowHideContainer = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'show-hide-container',template:/*ion-inline-start:"/Users/shaun/Mobile_Projects/Trident-Digital/src/components/show-hide-password/show-hide-password.html"*/'<ng-content></ng-content>\n<a class="type-toggle" (click)="toggleShow()">\n	<ion-icon class="show-option" [hidden]="show" name="eye"></ion-icon>\n	<ion-icon class="hide-option" [hidden]="!show" name="eye-off"></ion-icon>\n</a>\n'/*ion-inline-end:"/Users/shaun/Mobile_Projects/Trident-Digital/src/components/show-hide-password/show-hide-password.html"*/,
            host: {
                'class': 'show-hide-password'
            }
        }),
        __metadata("design:paramtypes", [])
    ], ShowHideContainer);
    return ShowHideContainer;
}());

//# sourceMappingURL=show-hide-container.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkConnectionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__message_message__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NetworkConnectionProvider = /** @class */ (function () {
    function NetworkConnectionProvider(platform, network, events, messageProvider) {
        this.platform = platform;
        this.network = network;
        this.events = events;
        this.messageProvider = messageProvider;
        this.isConnect = false;
        this.isOnToOff = false;
        this.isOffToOn = true;
    }
    NetworkConnectionProvider.prototype.init = function () {
        var _this = this;
        this.platform.ready().then(function () {
            var networkState = navigator.connection.type;
            var isConnect = false;
            if (networkState == Connection.NONE) {
                isConnect = false;
            }
            else {
                isConnect = true;
            }
            _this.isConnect = isConnect;
            _this.connected = _this.network.onConnect().subscribe(function (data) {
                _this.SetNetworkData(data.type, true, false, true);
            }, function (error) { return console.error(error); });
            _this.disconnected = _this.network.onDisconnect().subscribe(function (data) {
                _this.SetNetworkData(data.type, false, true, false);
            }, function (error) { return console.error(error); });
        });
    };
    NetworkConnectionProvider.prototype.SetNetworkData = function (type, isConnect, isOnToOff, isOffToOn) {
        this.isConnect = isConnect;
        this.isOnToOff = isOnToOff;
        this.isOffToOn = isOffToOn;
        this.displayNetworkUpdate(type);
        this.sendEvents(isConnect);
    };
    NetworkConnectionProvider.prototype.sendEvents = function (isConnect) {
        // this.events.publish('Sync:SetNetwork', isConnect);
    };
    NetworkConnectionProvider.prototype.displayNetworkUpdate = function (connectionState) {
        var error = "You are now " + connectionState;
        // this.messageProvider.showMessage(error);
    };
    NetworkConnectionProvider.prototype.releaseConnects = function () {
        this.connected.unsubscribe();
        this.disconnected.unsubscribe();
    };
    NetworkConnectionProvider.prototype.procNetworkError = function () {
        if (this.isConnect) {
            return false;
        }
        else {
            this.messageProvider.showMessage("You are now offline.");
            return true;
        }
        // return false;
    };
    NetworkConnectionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__message_message__["a" /* MessageProvider */]])
    ], NetworkConnectionProvider);
    return NetworkConnectionProvider;
}());

//# sourceMappingURL=network-connection.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HttpProvider = /** @class */ (function () {
    function HttpProvider(http) {
        this.http = http;
        this.BASE_URL = "https://redcape.venuesmedia.com/Pondhoppers_Trident/";
        this.LOGIN = this.BASE_URL + "Login";
        this.SLIDING_IMAGES = this.BASE_URL + "GetScreens";
        this.MENU_PAGE_DATE = this.BASE_URL + "Pages";
        this.STATUS = this.BASE_URL + "GetStatus";
    }
    HttpProvider.prototype.post = function (url, json) {
        var _this = this;
        console.log(JSON.stringify(json));
        return new Promise(function (resolve, reject) {
            _this.http.post(url, JSON.stringify(json))
                .subscribe(function (data) {
                console.log("success", JSON.stringify(data));
                resolve(data);
            }, function (err) {
                // console.log("error", JSON.stringify(err));
                reject(err);
            });
        });
    };
    HttpProvider.prototype.get = function (url) {
        var _this = this;
        console.log(JSON.stringify(url));
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(function (data) {
                // console.log("success", JSON.stringify(data));
                resolve(data);
            }, function (err) {
                // console.log("error", JSON.stringify(err));
                reject(err);
            });
        });
    };
    HttpProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], HttpProvider);
    return HttpProvider;
}());

//# sourceMappingURL=http.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessageProvider = /** @class */ (function () {
    function MessageProvider(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    MessageProvider.prototype.showMessage = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.present();
    };
    MessageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], MessageProvider);
    return MessageProvider;
}());

//# sourceMappingURL=message.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__http_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__network_connection_network_connection__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var GlobalProvider = /** @class */ (function () {
    function GlobalProvider(http, platform, transfer, file, events, network, storage) {
        this.http = http;
        this.platform = platform;
        this.transfer = transfer;
        this.file = file;
        this.events = events;
        this.network = network;
        this.storage = storage;
        // public EVENT_CONTENT_UPDATE = 'content_update';
        // public EVENT_PICTURE_UPDATE = 'picture_update';
        this.DEBUG = 0;
        this.INTERVAL_FOR_STATUS = 30;
        this.INTERVAL_FOR_GO_BACK = 60;
        this.KEY_CATEGORY = "category_data";
        this.KEY_STATUS = "status_data";
        this.status = { update_content: undefined, update_picture: undefined, statusCheckTime: 0, redirectTime: 0 };
        this.status.statusCheckTime = this.INTERVAL_FOR_STATUS;
        this.status.redirectTime = this.INTERVAL_FOR_GO_BACK;
        // this.getStatus();
    }
    GlobalProvider.prototype.getStatus = function (page_id) {
        var _this = this;
        if (page_id === void 0) { page_id = null; }
        return new Promise(function (resolve, reject) {
            var url = _this.http.STATUS;
            var jsonData = { venue_id: _this.user.id, token: _this.user.token, venDeviceId: _this.user.venDeviceId };
            _this.http.post(url, jsonData).then(function (data) {
                if (data) {
                    var res = data.json();
                    if (res && res.responseCode == "901") {
                        _this.status.statusCheckTime = res.statuscheckTime;
                        _this.status.redirectTime = res.redirectTime;
                        if (page_id === null || page_id === undefined) {
                            resolve(res.update_pictures == 'YES' ? true : false);
                        }
                        else {
                            if (res.updateRequired == 'YES' && res.pageUpdates && res.pageUpdates.length) {
                                res.pageUpdates.forEach(function (element) {
                                    var storedKey = _this.getStorageKey(element.pageId);
                                    var pageDatabase = element.pageDatabase == 'YES' ? true : false;
                                    var pageImages = element.pageImages == 'YES' ? true : false;
                                    // let storageData: any = this.getSavedData(storedKey);
                                    _this.getSavedData(storedKey).then(function (storageData) {
                                        if (storageData) {
                                            if (pageDatabase) {
                                                storageData.pageDatabase = pageDatabase;
                                            }
                                            if (pageImages) {
                                                storageData.pageImages = pageImages;
                                            }
                                        }
                                        _this.storage.set(storedKey, storageData);
                                    });
                                });
                            }
                            resolve(true);
                        }
                    }
                    else {
                        reject('no data');
                    }
                }
                else {
                    reject('no data');
                }
            }).catch(function () { return reject('no data'); });
        });
    };
    GlobalProvider.prototype.getStorageKey = function (id) {
        return this.KEY_CATEGORY + "_" + id;
    };
    GlobalProvider.prototype.getSavedData = function (key) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.get(key).then(function (v) {
                if (v) {
                    resolve(v);
                }
                else {
                    resolve(null);
                }
            }).catch(function (err) {
                resolve(null);
            });
        });
    };
    GlobalProvider.prototype.getBase64ImageData = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('android') || _this.platform.is('ios')) {
                if (_this.DEBUG == 1) {
                    console.log('mobile');
                }
                _this.download(url).then(function (result) { return resolve(result); }).catch(function (err) { return reject(err); });
            }
            else if (_this.platform.is('cordova')) {
                if (_this.DEBUG == 1) {
                    console.log('cordova');
                }
                _this.toDataURL(url).then(function (result) { return resolve(result); }).catch(function (err) { return reject(err); });
            }
            else {
                if (_this.DEBUG == 1) {
                    console.log('web');
                }
                _this.toDataURL(url).then(function (result) { return resolve(result); }).catch(function (err) { return reject(err); });
            }
        });
    };
    // this function is for web
    GlobalProvider.prototype.toDataURL = function (url) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    resolve(reader.result);
                };
                reader.onerror = function () {
                    reject(reader.error);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        });
    };
    // this function is for read device
    GlobalProvider.prototype.download = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fileTransfer = _this.transfer.create();
            fileTransfer.download(url, _this.file.cacheDirectory + _this.makeRandomString() + '.png').then(function (entry) {
                if (_this.DEBUG == 1) {
                    console.log('download complete: ' + entry.toURL());
                }
                _this.readBinaryFile(entry).then(function (result) {
                    resolve(result);
                }).catch(function (err) {
                    reject(err);
                });
            }, function (error) {
                console.log('download failure: ' + error);
                reject(error);
            });
        });
    };
    GlobalProvider.prototype.readBinaryFile = function (fileEntry) {
        return new Promise(function (resolve, reject) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function () {
                    // console.log("Successful file read: " + reader.result);
                    resolve(reader.result);
                };
                reader.onerror = function (err) {
                    // console.log("error file read: " + err);
                    reject(err);
                };
                reader.readAsDataURL(file);
            });
        });
    };
    GlobalProvider.prototype.makeRandomString = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 7; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };
    GlobalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* Events */], __WEBPACK_IMPORTED_MODULE_3__network_connection_network_connection__["a" /* NetworkConnectionProvider */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], GlobalProvider);
    return GlobalProvider;
}());

//# sourceMappingURL=global.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_message_message__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__category_category__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_network_connection_network_connection__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, network, storage, http, message, loadingCtrl, global) {
        this.navCtrl = navCtrl;
        this.network = network;
        this.storage = storage;
        this.http = http;
        this.message = message;
        this.loadingCtrl = loadingCtrl;
        this.global = global;
        this.KEY_SLIDING_IMAGES = "sliding_images";
        this.isDataFromServer = true;
        this.isFirstLoadingData = true;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.isFirstLoadingData = true;
        this.getData();
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.statusInterval = setInterval(function () {
            _this.isFirstLoadingData = false;
            _this.getData();
        }, this.global.status.statusCheckTime * 1000);
    };
    HomePage.prototype.ionViewWillLeave = function () {
        this.slides.stopAutoplay();
        clearInterval(this.statusInterval);
    };
    HomePage.prototype.getData = function () {
        var _this = this;
        if (this.network.procNetworkError()) {
            if (this.isFirstLoadingData) {
                this.getSlidingImagesFromLocal();
            }
        }
        else {
            this.global.getStatus().then(function (result) {
                if (result) {
                    _this.getSlidingImagesFromServer();
                }
                else if (_this.isFirstLoadingData) {
                    _this.getSlidingImagesFromLocal();
                }
            }).catch(function (err) {
                alert("You can't call the server API.\nPlease check your wifi.");
            });
        }
    };
    HomePage.prototype.getSlidingImagesFromServer = function () {
        var _this = this;
        if (this.global.DEBUG == 1) {
            console.log("getSlidingImagesFromServer...");
        }
        this.isDataFromServer = true;
        this.slidingImages = [];
        var loading = this.loadingCtrl.create();
        loading.present();
        var url = this.http.SLIDING_IMAGES;
        var jsonData = { venue_id: this.global.user.id, token: this.global.user.token, venDeviceId: this.global.user.venDeviceId };
        this.http.post(url, jsonData).then(function (data) {
            loading.dismiss();
            if (data) {
                var res = data.json();
                if (res && res.responseCode == "401" && res.ScreenData && res.ScreenData.length > 0) {
                    res.ScreenData.forEach(function (element, index) {
                        if (index > 0) {
                            _this.slidingImages.push(element);
                        }
                    });
                    _this.slidingImages.push(res.ScreenData[0]);
                    _this.slidingImages.splice(0, 0, _this.slidingImages[_this.slidingImages.length - 1]);
                    _this.slidesAutoPlay();
                    _this.saveToLocal();
                }
                else {
                    _this.noSavedDataOffline();
                }
            }
            else {
                _this.noSavedDataOffline();
            }
        }).catch(function (err) {
            loading.dismiss();
            if (_this.global.DEBUG == 1) {
                console.log(err);
            }
            _this.noSavedDataOffline();
        });
    };
    HomePage.prototype.getSlidingImagesFromLocal = function () {
        var _this = this;
        if (this.global.DEBUG == 1) {
            console.log("getSlidingImagesFromLocal...");
        }
        var loading = this.loadingCtrl.create();
        loading.present();
        this.isDataFromServer = false;
        this.slidingImages = [];
        this.storage.get(this.KEY_SLIDING_IMAGES).then(function (val) {
            loading.dismiss();
            if (val && val.length) {
                _this.slidingImages = val;
                _this.slidesAutoPlay();
            }
            else {
                _this.noSavedData();
            }
        }).catch(function (err) {
            loading.dismiss();
            if (_this.global.DEBUG == 1) {
                console.log(err);
            }
            _this.noSavedData();
        });
    };
    HomePage.prototype.saveToLocal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var images, i, element, imageData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.global.DEBUG == 1) {
                            if (this.global.DEBUG == 1) {
                                console.log('saveToLocal...');
                            }
                        }
                        images = [];
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < this.slidingImages.length)) return [3 /*break*/, 4];
                        element = this.slidingImages[i];
                        return [4 /*yield*/, this.global.getBase64ImageData(element.location)];
                    case 2:
                        imageData = _a.sent();
                        if (imageData) {
                            images.push({ id: element.id, imageData: imageData });
                        }
                        else {
                            images.push(element);
                        }
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        // Put drinks in fridge fridge = the data was saved to the local storage
                        // No Beers = not saved to the local
                        this.storage.set(this.KEY_SLIDING_IMAGES, images).then(function () { return console.log('Put drinks in fridge fridge'); }).catch(function () { return console.log('No Beers'); });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.noSavedData = function () {
        if (!this.network.procNetworkError()) {
            this.getSlidingImagesFromServer();
        }
    };
    HomePage.prototype.noSavedDataOffline = function () {
        alert('You must go online.');
        // navigator['app'].exitApp();
    };
    HomePage.prototype.slidesAutoPlay = function () {
        var _this = this;
        console.log("I got called");
        setTimeout(function () {
            _this.slides.freeMode = true;
            _this.slides.autoplay = 5000;
            _this.slides.speed = 1000;
            _this.slides.loop = true;
            _this.slides.autoplayDisableOnInteraction = false;
            _this.slides.startAutoplay();
        }, 100);
    };
    HomePage.prototype.goToCategoryPage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__category_category__["a" /* CategoryPage */], { pageId: 0 });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */])
    ], HomePage.prototype, "slides", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/home/home.html"*/'<ion-content class="blackback">\n  <ion-slides #slides class="image-slides">\n    <ion-slide *ngFor="let item of slidingImages">\n      <img [src]="isDataFromServer ? item.location : item.imageData" />\n    </ion-slide>\n  </ion-slides>\n  <div class="up-panel" (click)="goToCategoryPage()"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/shaun/Mobile_Projects/Trident-Digital/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_7__providers_network_connection_network_connection__["a" /* NetworkConnectionProvider */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_message_message__["a" /* MessageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[212]);
//# sourceMappingURL=main.js.map