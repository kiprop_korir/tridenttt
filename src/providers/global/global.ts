import { Injectable } from '@angular/core';
import { HttpProvider } from '../http/http';
import { Events, Platform } from 'ionic-angular';
import { NetworkConnectionProvider } from '../network-connection/network-connection';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Injectable()
export class GlobalProvider {
  // public EVENT_CONTENT_UPDATE = 'content_update';
  // public EVENT_PICTURE_UPDATE = 'picture_update';
  public DEBUG: number = 0;
  public INTERVAL_FOR_STATUS: number = 30;
  public INTERVAL_FOR_GO_BACK: number = 60;
  public KEY_CATEGORY = "category_data";
  public KEY_STATUS = "status_data";
  public user;
  public venDeviceId;
  public status = {update_content: undefined, update_picture: undefined, statusCheckTime: 0, redirectTime: 0};

  
  constructor (public http: HttpProvider, private platform: Platform, private transfer: FileTransfer, private file: File, public events: Events, public network: NetworkConnectionProvider, public storage: Storage) {
    this.status.statusCheckTime = this.INTERVAL_FOR_STATUS;
    this.status.redirectTime = this.INTERVAL_FOR_GO_BACK;
    // this.getStatus();
  }
  
  public getStatus(page_id: number = null) {
    return new Promise((resolve, reject) => {

      let url = this.http.STATUS;
      let jsonData: any = {venue_id: this.user.id, token: this.user.token, venDeviceId: this.user.venDeviceId};
      
      this.http.post(url, jsonData).then((data:any) => {
        if (data) {
          let res = data.json();
          if (res && res.responseCode == "901") {
            this.status.statusCheckTime = res.statuscheckTime;
            this.status.redirectTime = res.redirectTime;

            if (page_id === null || page_id === undefined) {
              resolve(res.update_pictures == 'YES' ? true : false);
            } else {
              if (res.updateRequired == 'YES' && res.pageUpdates && res.pageUpdates.length) {
                res.pageUpdates.forEach(element => {
                  let storedKey = this.getStorageKey(element.pageId);
                  let pageDatabase = element.pageDatabase == 'YES' ? true : false;
                  let pageImages = element.pageImages == 'YES' ? true : false;

                  // let storageData: any = this.getSavedData(storedKey);
                  this.getSavedData(storedKey).then((storageData: any) => {
                    if (storageData) {
                      if (pageDatabase) {
                        storageData.pageDatabase = pageDatabase;
                      }
                      if (pageImages) {
                        storageData.pageImages = pageImages;
                      }
                    }
                    this.storage.set(storedKey, storageData);
                  });
                });
              }

              resolve(true);
            }
          } else {
            reject('no data');
          }
        } else {
          reject('no data');
        }
      }).catch(() => reject('no data'));
  
    });
  }

  public getStorageKey(id: string) {
    return this.KEY_CATEGORY + "_" + id;
  }

  public getSavedData(key: string) {
    return new Promise((resolve) => {
      this.storage.get(key).then((v) => {
        if (v) {
          resolve(v);
        } else {
          resolve(null);
        }
      }).catch((err) => {
        resolve(null);
      });
    });
  }

  public getBase64ImageData(url) {
    return new Promise((resolve, reject) => {
      if (this.platform.is('android') || this.platform.is('ios')) {
        if(this.DEBUG == 1) {
          console.log('mobile');
        }
        this.download(url).then(result => resolve(result)).catch(err => reject(err));
      } else if (this.platform.is('cordova')) {
        if(this.DEBUG == 1) {
          console.log('cordova');
        }
        this.toDataURL(url).then(result => resolve(result)).catch(err => reject(err));
      } else {          // test code for ionic serve
        if(this.DEBUG == 1) {
          console.log('web');
        }
        this.toDataURL(url).then(result => resolve(result)).catch(err => reject(err));
      }
    });
  }
  
  // this function is for web
  private toDataURL(url) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
          resolve(reader.result);
        }
        reader.onerror = function () {
          reject(reader.error);
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
    });
  }

  // this function is for read device
  private download(url) {
    return new Promise((resolve, reject) => {
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.download(url, this.file.cacheDirectory + this.makeRandomString() + '.png').then((entry) => {
        if(this.DEBUG == 1) {
          console.log('download complete: ' + entry.toURL());
        }
        this.readBinaryFile(entry).then((result) => {
          resolve(result);
        }).catch((err) => {
          reject(err);
        });
      }, (error) => {
        
          console.log('download failure: ' + error);
        
        reject(error);
      });
    });
  }

  private readBinaryFile(fileEntry) {
    return new Promise((resolve, reject) => {
      fileEntry.file(function (file) {
        let reader = new FileReader();
        reader.onloadend = function() {
          // console.log("Successful file read: " + reader.result);
          resolve(reader.result);
        };
        reader.onerror = function(err: any) {
          // console.log("error file read: " + err);
          reject(err);
        }
        reader.readAsDataURL(file);
      });
    });
  }

  private makeRandomString() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 7; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
  
    return text;
  }

}
