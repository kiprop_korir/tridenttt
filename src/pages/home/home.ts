import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, Slides } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { MessageProvider } from '../../providers/message/message';
import { GlobalProvider } from '../../providers/global/global';
import { CategoryPage } from '../category/category';
import { Storage } from '@ionic/storage';
import { NetworkConnectionProvider } from '../../providers/network-connection/network-connection';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  KEY_SLIDING_IMAGES = "sliding_images";
  @ViewChild(Slides) slides: Slides;
  slidingImages;
  slidesInterval;
  isDataFromServer = true;
  statusInterval;
  isFirstLoadingData: boolean = true;
  
  constructor(public navCtrl: NavController, public network: NetworkConnectionProvider, public storage: Storage, public http: HttpProvider, public message: MessageProvider, public loadingCtrl: LoadingController, public global: GlobalProvider) {
  }

  ionViewDidLoad() {
    this.isFirstLoadingData = true;
    this.getData();
  }
  
  ionViewDidEnter() {
    this.statusInterval = setInterval(() => {
      this.isFirstLoadingData = false;
      this.getData();
    }, this.global.status.statusCheckTime * 1000);
  }

  ionViewWillLeave() {
    this.slides.stopAutoplay();
    clearInterval(this.statusInterval);
  }

  getData() {
    if (this.network.procNetworkError()) {
      if (this.isFirstLoadingData) {
        this.getSlidingImagesFromLocal();
      }
    } else {
      this.global.getStatus().then((result) => {
        if (result) {
          this.getSlidingImagesFromServer();
        } else if (this.isFirstLoadingData) {
          this.getSlidingImagesFromLocal();
        }
      }).catch((err) => {
        alert("You can't call the server API.\nPlease check your wifi.");
      });
    }
  }
  
  getSlidingImagesFromServer() {
    if(this.global.DEBUG == 1) {
      console.log("getSlidingImagesFromServer...");
    }

    this.isDataFromServer = true;
    this.slidingImages = [];

    let loading = this.loadingCtrl.create();
    loading.present();

    let url = this.http.SLIDING_IMAGES;
    let jsonData: any = {venue_id: this.global.user.id, token: this.global.user.token, venDeviceId: this.global.user.venDeviceId};
    
    this.http.post(url, jsonData).then((data:any) => {
      loading.dismiss();
      if (data) {
        let res = data.json();
        if (res && res.responseCode == "401" && res.ScreenData && res.ScreenData.length > 0) {
          res.ScreenData.forEach((element, index) => {
            if (index > 0) {
              this.slidingImages.push(element);
            }
          });
          this.slidingImages.push(res.ScreenData[0]);
          this.slidingImages.splice(0, 0, this.slidingImages[this.slidingImages.length - 1]);
          this.slidesAutoPlay();
          this.saveToLocal();
        } else {
          this.noSavedDataOffline();
        }
      } else {
        this.noSavedDataOffline();
      }
    }).catch((err) => {
      loading.dismiss();
      if(this.global.DEBUG == 1) {
        console.log(err);
      }
      this.noSavedDataOffline();
    });
  }
  
  getSlidingImagesFromLocal() {
    if(this.global.DEBUG == 1) {
      console.log("getSlidingImagesFromLocal...");
    }
    let loading = this.loadingCtrl.create();
    loading.present();

    this.isDataFromServer = false;
    this.slidingImages = [];

    this.storage.get(this.KEY_SLIDING_IMAGES).then(val => {
      loading.dismiss();
      if (val && val.length) {
        this.slidingImages = val;
        this.slidesAutoPlay();
      } else {
        this.noSavedData();
      }
    }).catch(err => {
      loading.dismiss();
      if(this.global.DEBUG == 1) {
      console.log(err);
      }
      this.noSavedData();
    });
  }

  async saveToLocal() {
    if(this.global.DEBUG == 1) {
      if(this.global.DEBUG == 1) {
        console.log('saveToLocal...');
      }
    }

    let images: any = [];
    for (let i = 0; i < this.slidingImages.length; i ++) {
      let element = this.slidingImages[i];
      let imageData = await this.global.getBase64ImageData(element.location);
      if (imageData) {
        images.push({id: element.id, imageData});
      } else {
        images.push(element);
      }
    }
    // Put drinks in fridge fridge = the data was saved to the local storage
    // No Beers = not saved to the local
    this.storage.set(this.KEY_SLIDING_IMAGES, images).then(() => console.log('Put drinks in fridge fridge')).catch(() => console.log('No Beers'));
  }

  noSavedData() {
    if (!this.network.procNetworkError()) {
      this.getSlidingImagesFromServer();
    }
  }

  noSavedDataOffline() {
    alert('You must go online.');
    // navigator['app'].exitApp();
  }
  
  slidesAutoPlay() {
    console.log("I got called");
    setTimeout(() => {
      this.slides.freeMode = true;
      this.slides.autoplay = 5000;
      this.slides.speed = 1000;
      this.slides.loop = true;
      this.slides.autoplayDisableOnInteraction = false;
      this.slides.startAutoplay();
    }, 100);
  }
  
  goToCategoryPage() {
    this.navCtrl.setRoot(CategoryPage, {pageId: 0});
  }
}
