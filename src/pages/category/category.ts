import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, ModalController, Events } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { MessageProvider } from '../../providers/message/message';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';
import { DetailsPage } from '../details/details';
import { Storage } from '@ionic/storage';
import { NetworkConnectionProvider } from '../../providers/network-connection/network-connection';
import { DebugContext } from '@angular/core/src/view';

// declare var cordova;
// declare var window;

@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  pageId: number;
  pageParent: number;
  typeOfPage: string;
  displayStyle: string;
  productDetail: string;
  backgroundImage: string;
  headerImage: string;
  backgroundImageData: string;
  headerImageData: string;
  categories: any = [];
  timeInterval;
  statusInterval;
  closeTimeCount: number = 2;
  isDataFromServer = true;
  isFirstLoadingData = true;
  storedData;
  modalProduct;
  
  constructor(public navCtrl: NavController, public network: NetworkConnectionProvider, public storage: Storage, public events: Events, public navParams: NavParams, public http: HttpProvider, public message: MessageProvider, public loadingCtrl: LoadingController, public global: GlobalProvider, public modalCtrl: ModalController) {
    this.pageId = this.navParams.get('pageId');
    this.isFirstLoadingData = true;
    this.getData();
  }

  ionViewDidEnter() {
    this.statusInterval = setInterval(() => {
      this.isFirstLoadingData = false;
      this.getData();
    }, this.global.status.statusCheckTime * 1000);

    // if (this.pageId === 0) {
      this.timeInterval = setInterval(this.countCloseTime.bind(this), 1000);
    // }
  }

  ionViewWillLeave() {
    clearInterval(this.timeInterval);
    clearInterval(this.statusInterval);
  }

  countCloseTime() {
    if (this.closeTimeCount ++ >= this.global.status.redirectTime) {
      if (this.modalProduct) {
        this.modalProduct.dismiss();
      }
      this.goToHomePage();
    }
  }

  getData() {
    if (this.network.procNetworkError()) {
      if (this.isFirstLoadingData) {
        // this.storedData = this.global.getSavedData(this.global.getStorageKey(this.pageId.toString()));
        this.global.getSavedData(this.global.getStorageKey(this.pageId.toString())).then((s) => {
          this.storedData = s;
          this.getPageDataFromLocal();
        });
      }
    } else {
      this.global.getStatus(this.pageId).then((result: any) => {
        // this.storedData = this.global.getSavedData(this.global.getStorageKey(this.pageId.toString()));
        this.global.getSavedData(this.global.getStorageKey(this.pageId.toString())).then((s) => {
          this.storedData = s;
  
          if (this.storedData == null || this.storedData.pageDatabase || this.storedData.pageImages) {
            this.getPageDataFromServer();
          } else if (this.isFirstLoadingData) {
            this.getPageDataFromLocal();
          }
        });
      }).catch((err) => {
        this.message.showMessage("You can't call the server API.\nPlease check your wifi.");
        if (this.storedData) {
          this.getPageDataFromLocal();
        } else {
          this.goReturnPage();
        }
      });;

    }
  }

  getPageDataFromLocal() {
    if(this.global.DEBUG == 1) {
      console.log('getPageDataFromLocal');
    }
    this.isDataFromServer = false;

    if (this.storedData && this.storedData.pageData && this.storedData.pageInfo) {
      this.pageParent = this.storedData.pageData.pageParent;
      this.backgroundImageData = this.storedData.pageData.backgroundImageData;
      this.headerImageData = this.storedData.pageData.headerImageData;
      this.typeOfPage = this.storedData.pageData.typeOfPage;
      this.displayStyle = this.storedData.pageData.displayStyle;
      this.categories = this.storedData.pageInfo;
      this.setBackgroundImage();
    } else {
      this.noSavedData();
    }
  }

  getPageDataFromServer() {
    if(this.global.DEBUG == 1) {
      console.log('getPageDataFromServer');
    }
    this.isDataFromServer = true;

    let url = this.http.MENU_PAGE_DATE;
    let jsonData: any = {venue_id: this.global.user.id, token: this.global.user.token, venDeviceId: this.global.user.venDeviceId};

    if (this.pageId > 0) {
      jsonData.page_id = this.pageId;
    }
    let categories = [];

    let loading = this.loadingCtrl.create({content: 'Loading Please Wait...'});
    loading.present();

    this.http.post(url, jsonData).then((data: any) => {
      loading.dismiss();
      if (data) {
        let res = data.json();
        if (res && res.responseCode == "401" && res.pageData && res.pageInfo) {
          this.backgroundImage = res.pageData.pageBackground;
          this.headerImage = res.pageData.pageHeader;
          this.pageParent = res.pageData.pageParent;
          this.typeOfPage = res.pageData.typeOfPage;
          this.displayStyle = res.pageData.displayStyle;
          this.setBackgroundImage();

          let arr = [];
          for (let i = 0; i < res.pageInfo.length; i ++) {
             arr.push(res.pageInfo[i]);
          }
          categories.push(arr);
          this.categories = categories;
          this.saveToLocal();
        } else {
          this.noSavedDataOffline();
        }
      } else {
        this.noSavedDataOffline();
      }
    }).catch((err) => {
      loading.dismiss();
      if(this.global.DEBUG == 1) {
        console.log(err);
      }
      this.noSavedDataOffline();
    });
  }
  
  async saveToLocal() {
    if(this.global.DEBUG == 1) {
      console.log('saveToLocal...');
    }
    let backgroundImageData;
    let headerImageData;
    if (this.storedData == null || this.storedData.pageImages) {
      backgroundImageData = await this.global.getBase64ImageData(this.backgroundImage);
      headerImageData = await this.global.getBase64ImageData(this.headerImage);
    } else {
      backgroundImageData = this.storedData.pageInfo.backgroundImageData;
      headerImageData = this.storedData.pageInfo.headerImageData;
    }

    let categories: any = [];
    for (let i = 0; i < this.categories.length; i ++) {
      let elements1 = [];
      let elements = this.categories[i];
      for (let j = 0; j < elements.length; j ++) {
        let element = elements[j];
        if (element.categoryImage && (this.storedData == null || this.storedData.pageImages)) {
          element.categoryImageData = await this.global.getBase64ImageData(element.categoryImage);
        }
        elements1.push(element);
      }
      categories.push(elements1);
    }

    let key = this.global.getStorageKey(this.pageId.toString());
    let pageData = {pageId: this.pageId, pageParent: this.pageParent, backgroundImageData, headerImageData, typeOfPage: this.typeOfPage, displayStyle: this.displayStyle};
    // Got drink from fridge = the data was saved to the local storage
    // Fridge was empty = not saved to the local
    this.storage.set(key, {pageData, pageInfo: categories}).then(() => console.log("Got drink from fridge, " + key)).catch(() => console.log("Fridge was empty"));
    this.storedData = {pageData, pageInfo: categories};
  }

  noSavedData() {
    this.message.showMessage('There is no saved data on this device for this page.');
    if (this.network.procNetworkError) {
      this.getPageDataFromServer();
    }
  }

  noSavedDataOffline() {
    this.message.showMessage('There is no data for this page.');
    this.goReturnPage();
  }

  goReturnPage() {
    if (this.pageId === 0) {
      this.goToHomePage();
    } else {
      this.goBack();
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  goMain() {
    this.navCtrl.popToRoot();
  }

  goToHomePage() {
    this.navCtrl.setRoot(HomePage);
  }

  goToCategoryPage(pageId) {
    if (pageId > 0) {
      this.closeTimeCount = 0;
      this.navCtrl.push(CategoryPage, {pageId: pageId});
    }
  }

  openModal(pageInfo) {
    this.closeTimeCount = 0;
    this.modalProduct = this.modalCtrl.create(DetailsPage, {pageInfo: pageInfo});
    this.modalProduct.onDidDismiss(data => {
      this.closeTimeCount = 0;
    });
    this.modalProduct.present();
  }

  setBackgroundImage() {
    setTimeout(() => {
      if (this.isDataFromServer) {
        if(this.global.DEBUG == 1) {
        console.log(this.backgroundImage);
        }
        document.getElementById("category-content-" + this.pageId.toString()).style["background-image"] = "url('" + this.backgroundImage + "')";
      } else {
        if(this.global.DEBUG == 1) {
          console.log(this.backgroundImageData);
        }
        document.getElementById("category-content-" + this.pageId.toString()).style["background-image"] = "url(" + this.backgroundImageData + ")";
      }
    }, 100);
  }

  unpin() {
    window["plugins"].locktask.stopLockTask(null, null);
    
  }

}
